$(document).ready(function() {

    /**
     * Удаление товара из корзины
     */
    $('body').on("click", '.content__basket-table_tovar-delete', function(event) {
        event.preventDefault();

        let idTovar = $(this).closest("tr").attr('data-id');

        $.ajax({
            type: "POST",
            url: "index.php",
            data: {
                idTovar:idTovar,
                command:"deleteTovar"
            }
        }).done(function(data) {
            let tovar = JSON.parse(data);
                $('.header__cart_shape span').text(tovar.coll);
                $('.header__cart_sum span').text(tovar.cost + ' р');
                $('.content__basket__prise').text(tovar.cost);
                if(tovar.coll == 0) {
                    function goToMain() {
                        $(location).attr('href','/');
                    }
                    setTimeout(goToMain, 500);
                }
        });
        $(this).closest("tr").fadeOut(555);
    })

    /**
     * Добавление товара
     */
    $('body').on("click", '.content__tovar_buy, .content__cartochka-tovar_to-basket', function(event) {
        event.preventDefault();

        let idTovar = $(this).attr('data-id');
        let collTovar = $('#content__cartochka-tovar_coll-input').val();

        if(!collTovar){
            collTovar = 1;
        }
        if($(this).hasClass('similar')) {
            collTovar = 1;
        }

        if(collTovar > 0){
            $.ajax({
                type: "POST",
                url: "index.php",
                data: {
                    idTovar: idTovar,
                    command: "addTovarToBasketInCatalog",
                    collTovar: collTovar
                }
            }).done(function(data) {
                let tovar = JSON.parse(data);
                $('.header__cart_sum span').text(tovar.cost + ' р');
                $('.header__cart_shape span').text(tovar.coll);
                $('.alert_modal')
                    .css('display', 'block')
                    .animate({opacity: 1, top: '5%'}, 200);
                    setTimeout(function () {
                    $('.alert_modal')
                        .animate({opacity: 0, top: '3%'}, 200,
                            function () {
                                $(this).css('display', 'none');
                            }
                        );
                }, 700);
            });
        }
    })

    /**
     * Очистка всей корзины
     */
    $('body').on("click", '.content__basket-table_basket-clear', function(event) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "index.php",
            data: {
                command:"clearBasket"
            }
        }).done(function(data) {
            let tovar = JSON.parse(data);
            $('.header__cart_sum span').text(tovar.cost + ' р');
            $('.header__cart_shape span').text(tovar.coll);
            $('.content__basket__prise').text(tovar.cost);

            function clearBasketFunc() {
                $(".content__basket-table tbody tr").detach();
            }
            setTimeout(clearBasketFunc, 250);

            function goToMain() {
                $(location).attr('href','/');
            }
            setTimeout(goToMain, 500);
        });
    });

    /**
     * Переход на страницу корзины
     */
    $('body').on("click", '.header__logo_cat, .header__cart_shape', function(event) {
        event.preventDefault();
        $(location).attr('href','/basket');
    })


    /**
     * Редактирование товара в корзине
     */
    $('body').on("change", '.content__basket-table_tovar-coll', function(event) {
        event.preventDefault();

        let idTovar = $(this).attr('data-id');
        let collTovar = $(this).val();

        $.ajax({
            type: "POST",
            url: "index.php",
            data: {
                idTovar: idTovar,
                command: "updateTovar",
                collTovar: collTovar
            }
        }).done(function(data) {
         let tovar = JSON.parse(data);
            $('.header__cart_sum span').text(tovar.cost + ' р');
            $('.header__cart_shape span').text(tovar.coll);
            $('.td-total'+idTovar).text(tovar.productTotal + ' р.');
            $('.content__basket__prise').text(tovar.cost);
        });
    })
});


