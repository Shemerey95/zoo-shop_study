<?php
namespace application\models;
use application\core\Db;


/* 
 * Модель для работы с корзиной
 * @author PC16
 */
class BasketModel {
    
    private $DBconn;
    private static $basketARRAY;
    private static $userId;


    public function __construct() {
        $this->DBconn = Db::getInstance();
        if(isset($_COOKIE['USER_ID']))
            $this->userId = $_COOKIE['USER_ID'];
        else
            $this->userId = NULL;
    }

    // Добавить JSON с корзиной (и ID сессии пользователя)
    public function addBasket($userId, $cartJSON) {
        $preparedString = "SELECT `session_id` FROM `order` WHERE session_id=?";
        $paramsTypes = "s";
        $paramValues = array($userId);

        $basketExists = $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
        if(!$basketExists) {
            $preparedString = "INSERT INTO `order` VALUES(DEFAULT, ?, ?)";
            $paramsTypes = "ss";
            $paramValues = array($userId, $cartJSON);
        }
        else {
            $preparedString = "UPDATE `order` SET items=? WHERE session_id=?";
            $paramsTypes = "ss";
            $paramValues = array($cartJSON, $userId);
        }
        $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
    }
    // Добавить товар в корзину
    // Возвращает общую сумму по корзине
    public function addProduct($productId, $count) {
        // если кэш корзины пуст
        if(!self::$basketARRAY) {
            // получить корзину из базы
            $this->getBasket($this->userId);
        }
        if($this->isEmpty()) {
            // добавить $count единиц нового товара
            self::$basketARRAY[$productId] = $count;
            // сохранить корзину в базу
            $basketJSON = json_encode(self::$basketARRAY, JSON_UNESCAPED_UNICODE);
            $this->addBasket($this->userId, $basketJSON);
            return $this->basketTotalPrice(self::$basketARRAY);
        }
        // иначе проверяем, есть ли товар в корзине
        $productCount = $this->productsCountById($productId, self::$basketARRAY);
        
        if($productCount == 0) {
            // если нет такого товара
            self::$basketARRAY[$productId] = $count;
        }
        else {
            // если есть
            self::$basketARRAY[$productId] = self::$basketARRAY[$productId] + $count;
        }
        $basketJSON = json_encode(self::$basketARRAY, JSON_UNESCAPED_UNICODE);
        $this->addBasket($this->userId, $basketJSON);
        return $this->basketTotalPrice(self::$basketARRAY);
    }
    public function updateProduct($productId, $count) {
        if(!self::$basketARRAY) {
            $this->getBasket($this->userId);
        }
        self::$basketARRAY[$productId] = $count;
        $basketJSON = json_encode(self::$basketARRAY, JSON_UNESCAPED_UNICODE);
        $this->addBasket($this->userId, $basketJSON);
        return $this->productTotalPrice($productId, self::$basketARRAY);
    }
    
    // Удалить товар из корзины
    // Возвращает общую сумму по корзине
    public function deleteProduct($productId) {
        if(!self::$basketARRAY) {
            $this->getBasket($this->userId);
        }

        unset(self::$basketARRAY[$productId]);

        $basketJSON = json_encode(self::$basketARRAY, JSON_UNESCAPED_UNICODE);
        $this->addBasket($this->userId, $basketJSON);
        return $this->basketTotalPrice(self::$basketARRAY);
    }
    // Очистка корзины
    public function clear() { 
        $preparedString = "UPDATE `order` SET `items`='[]' WHERE session_id=?";
        $paramsTypes = "s";
        $paramValues = array($this->userId);

        $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
        self::$basketARRAY = array();
        return 0;
    }
    
    // Пустая ли корзина
    public function isEmpty() {
        if(!self::$basketARRAY || self::$basketARRAY == '[]')
            return TRUE;
        return FALSE;
    }
    
    // Получить массив с корзиной по ID сессии пользователя
    public function getBasket($userId) {
        $preparedString = 'SELECT `items` FROM `order` WHERE session_id=?';
        $paramsTypes = "s";
        $paramValues = array($userId);
        $basketExists = $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
        
        $basketARRAY = array();
        if($basketExists) {
            $basketARRAY = json_decode($basketExists[0]['items'], JSON_UNESCAPED_UNICODE);
        }
        $_SESSION['BASKET'] = $basketARRAY;
        self::$basketARRAY = $basketARRAY;
        return self::$basketARRAY;
    }
    // Получить массив идентификаторов товаров из корзины
    public function getProductsIds($basketARRAY) {
        if($basketARRAY) {
            return array_keys($basketARRAY);
        }
        return [];
    }
    // Получить товары из базы по идентификаторам для корзины
    // $ids - массив айди товаров из корзины
    // в результрующем массиве - поля из базы - ИД, ФОТО, ИМЯ, ЦЕНА
    public function getProductsByIds($ids) {
        if($ids) {
            $ids = implode("', '", $ids);
            $preparedString = "SELECT `id`, `photo`, `name`, `price` FROM `product` WHERE `id` IN ('$ids')";
            return $products = $this->DBconn->preparedQuery($preparedString);
        }
        return [];
    }
    // Получить готовый массив товаров с доп. полями количества и суммы для отображения на странице корзины
    // $sessionId - айди сессии пользователя
    // в результрующем массиве - поля из базы - ИД, ФОТО, ИМЯ, ЦЕНА, КОЛИЧЕСТВО, СУММА
    public function getProductsList($sessionId) {
        if (!self::$basketARRAY) {
            $this->getBasket($sessionId);
        }
        $ids = $this->getProductsIds(self::$basketARRAY);
        if($ids) {
            $productsARRAY = $this->getProductsByIds($ids);
            if($productsARRAY) {
                foreach ($productsARRAY as $index => $product) {
                    $productsARRAY[$index]['count'] = $this->productsCountById($product['id'], self::$basketARRAY);
                    $productsARRAY[$index]['total_price'] = $this->productTotalPrice($product['id'], self::$basketARRAY);
                }
            }
            return $productsARRAY;
        }
        return [];
    }
    

    // Сколько наименований в корзине   
    public function productsCount($basketARRAY) {
        return count($basketARRAY);
    }
    // Сколько единиц товаров в корзине
    public static function productItemsCount() {
        $productsCount = 0;
        if(!self::$basketARRAY) {
            if(isset($_SESSION['BASKET'])) {
                self::$basketARRAY = $_SESSION['BASKET'];
            }
            else {
                return $productsCount;
            }
        }
        foreach (self::$basketARRAY as $productCount) {
            $productsCount += $productCount;
        }
        return $productsCount;
    }
    // Сколько единиц товара определенного ID в корзине 
    public function productsCountById($productId, &$basketARRAY) {
        if($basketARRAY) {
            if(key_exists($productId, $basketARRAY)) {
                return intval($basketARRAY[$productId]);
            }
        }
        return 0;
    }
    // Общая сумма по определенному товару в корзине
    public function productTotalPrice($productId, &$basketARRAY) {
        $preparedString = "SELECT `price` FROM `product` WHERE id=?";
        $paramsTypes = "i";
        $paramValues = array($productId);
        $productPrice = $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
        $productPrice = $productPrice[0]['price'];
        return $productPrice * ($this->productsCountById($productId, $basketARRAY));
    }
    // Посчитать общую сумму для корзины
    public function basketTotalPrice() {
        $totalPrice = 0;
        if(!self::$basketARRAY) {
            $this->getBasket($this->userId);
        }
        if($this->isEmpty()) {
            return $totalPrice;
        }
        foreach (self::$basketARRAY as $productId => $productCount) {
            $totalPrice += $this->productTotalPrice($productId, self::$basketARRAY);
        }
        return $totalPrice;
    }
}
