<?php
/**
 * Класс CatalogModel
 * Модель для каталога
 */



namespace application\models;
use application\core\Model;
use application\core\XMLFileParser;

require_once $_SERVER['DOCUMENT_ROOT'] . '/application/core/XMLFileParser.class.php';


class CatalogModel extends Model
{

    /**
     * Функция для получения массива похожих товаров
     */

    public function getSimilarProducts($id) {
        $array_similars = [];
        $array_products = $this->getProductsByCategory($id);
        $count_array_products = count($array_products);
        for($i=0; $i < 5; $i++) {
            $rand = rand(1, $count_array_products-1);
            $array_similars[] = $array_products["$rand"];
        }
        return $array_similars;
    }


    /**
     * Функция для получения товаров по id секции
     */

    public function getProductsByIdSection($id) {
        $result_product=$this->DBconn->query("SELECT * FROM `product` WHERE `section_id` = $id");
        $all_products = [];
        while ($row_product = $result_product->fetch_array()) {
            $all_products[] = $row_product;
        }

        return $all_products;
    }

    /**
     * Функция для получения id секции по имени
     */

    public function getIdSectionByName($name) {
        $name = lcfirst($name);
        $result_product=$this->DBconn->query("SELECT `id` FROM `section` WHERE `route` = '$name'");
        while ($row_product = $result_product->fetch_array()) {
            $id_section = $row_product['id'];
        }
        return  $id_section;

    }

    /**
     * Функция для получения товаров текущей страницы
     */

    public function postList($route,$id){
        $max = 20;
        if (!isset($route['page'])) {
            $route['page'] = 1;
        }
        $start = ($route['page'] - 1) * $max;
        $result_product = $this->DBconn->query("SELECT * FROM `product` WHERE `section_id` = $id LIMIT $start, $max ");
        $all_products = [];
        while ($row_product = $result_product->fetch_assoc()) {
            $all_products[] = $row_product;
        }
        return $all_products;
    }

    /**
     * Функция для заполнения базы
     */
    public function initCatalog() {
        $parser = new XMLFileParser($_SERVER['DOCUMENT_ROOT'] . '/application/include/catalog.xml');

        $parsedCatalog = $parser->getSeparateArrays();

        $products = $parser->getProducts();
        $sections = $parser->getSections();
        $suppliers = $parser->getSuppliers();
        $purposes = $parser->getPurposes();

        $this->updateSuppliers($suppliers);
        $this->updatePurposes($purposes);
        /*$this->updateSections($sections); Метод заккоментирован чтобы вручную не добавлять значения т.к Дима не прописал роуты */
        $this->updateProducts($products);

        return;
    }

    // Получить все товары
    public function getProducts() {
        $preparedString = 'SELECT * FROM product';
        return $this->DBconn->preparedQuery($preparedString);
    }

    // Получить все характеристики
    public function getCharacteristics() {
        $preparedString = 'SELECT * FROM characteristics_dict';
        return $this->DBconn->preparedQuery($preparedString);
    }
    // Получить товар по ID товара
    public function getProductById($id) {
        $preparedString = 'SELECT * FROM product WHERE id=?';
        $paramsTypes = "i";
        $paramValues = array($id);

        $popduct = $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);

        $purpose = $this->getPurposeBy($id);
        $supplier = $this->getSupplierBy($id);

        $foreign_info = [
            "purpose" => "$purpose",
            "supplier" => "$supplier",
        ];
        if($popduct)
            $all_product_info = array_merge($popduct['0'], $foreign_info);

        return $all_product_info;
    }
    // Получить товары определенной категории
    public function getProductsByCategory($category) {
        $preparedString = 'SELECT * FROM product WHERE section_id=?';
        $paramsTypes = "i";
        $paramValues = array($category);
        return $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
    }
    // Получить назначение товара по ID
    // return строка с именем назначения
    public function getPurposeBy($productId) {
        $preparedString = "SELECT purpose.name FROM product JOIN purpose ON purpose.id=product.purpose_id WHERE product.id='$productId'";
        $res = $this->DBconn->preparedQuery($preparedString);
        return $res[0]['name'];
    }

    // Получить производителя товара по ID
    // return строка с именем производителя
    public function getSupplierBy($productId) {
        $preparedString = "SELECT supplier.name FROM product JOIN supplier ON supplier.id=product.supplier_id WHERE product.id='$productId'";
        $res = $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
        return $res[0]['name'];
    }



    // возвращает массив айдишников продуктов из базы
    private function getProductIdsDB() {
        $preparedString = 'SELECT id FROM product';
        $ids = $this->DBconn->preparedQuery($preparedString);
        $idsDB = [];
        if($ids){
            foreach ($ids as $id) {
                $idsDB[] = $id['id'];
            }
        }
        return $idsDB;
    }
    // Обновить разделы
    // ++++++++++++++++++++++++++++++++++++++
    public function updateSections($sections){
        if($sections)
            foreach ($sections as $index => $name)
                if($name)
                    $this->addSection($name);
    }
    // Обновить поставщиков
    // ++++++++++++++++++++++++++++++++++++++
    public function updateSuppliers($suppliers){
        if($suppliers)
            foreach ($suppliers as $index => $name)
                if($name)
                    $this->addSupplier($name);
    }
    // Обновить назначения
    // ++++++++++++++++++++++++++++++++++++++
    public function updatePurposes($purposes){
        if($purposes)
            foreach ($purposes as $index => $name)
                if($name)
                    $this->addPurpose($name);
    }
    // ++++++++++++++++++++++++++++++++++++++
    public function addSupplier($name){
        $name = trim(strip_tags($name));
        $preparedString = "SELECT * FROM purpose WHERE name='$name'";
        $supplierExists = $this->DBconn->preparedQuery($preparedString);

        if(!$supplierExists){
            $preparedString = "INSERT INTO supplier VALUES(DEFAULT, '$name')";
            $this->DBconn->preparedQuery($preparedString);
        }
    }
    // ++++++++++++++++++++++++++++++++++++++
    public function addPurpose($name){
        $name = trim(strip_tags($name));
        $preparedString = "SELECT * FROM purpose WHERE name='$name'";
        $purposeExists = $this->DBconn->preparedQuery($preparedString);

        if(!$purposeExists) {
            $preparedString = "INSERT INTO purpose VALUES(DEFAULT, '$name')";
            $this->DBconn->preparedQuery($preparedString);
        }
    }
    // Добавить раздел из распарсенного объекта (по умолчанию - два поля)
    // дополнительно можно передать маршрут и фото
    // ++++++++++++++++++++++++++++++++++++++
    public function addSection($section, $route = "", $photo = ""){
        $idIndex = 0;
        $nameIndex = 1;
        $id = $section[$idIndex]['ID'];
        $preparedString = 'SELECT id FROM section WHERE id=?';
        $paramsTypes = "i";
        $paramValues = array($id);
        $sectionExists = $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
        if(!$sectionExists) {
            $name = $section[$nameIndex]['NAME'];
            $name = trim(strip_tags($name));
            $preparedString = 'INSERT INTO section VALUES(?, ?, ?, ?)';
            $paramsTypes = "isss";
            $paramValues = array($id, $name, $route, $photo);
            $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
        }
    }
    // Добавить раздел из ассоциативного массива
    // ++++++++++++++++++++++++++++++++++++++
    public function addSectionArray(array $section){
        if($section) {
            $id = $section['id'];
            $preparedString = 'SELECT id FROM section WHERE id=?';
            $paramsTypes = "i";
            $paramValues = array($id);
            $sectionExists = $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
            if(!$sectionExists) {
                $name = $section['name'];
                $route = $section['route'];
                $photo = $section['photo'];
                $preparedString = 'INSERT INTO section VALUES(?, ?, ?, ?)';
                $paramsTypes = "isss";
                $paramValues = array($id, $name, $route, $photo);
                $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
            }
        }
    }

    // Добавить новый товар в БД
    // ++++++++++++++++++++++++++++++++++++++
    private function addProduct($product){
        $productFields = $this->getProductFields($product);
        $preparedString = 'INSERT INTO product VALUES (?, ?, ?, ?, ?, ?, ?, ?);';
        $paramsTypes = "iisissii";
        $paramValues = array(
            $productFields['id'],
            $productFields['section_id'],
            $productFields['name'],
            $productFields['price'],
            $productFields['desription'],
            $productFields['photo'],
            $productFields['purpose_id'],
            $productFields['supplier_id']
        );
        $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
    }
    // Обновить поля одного товара в БД
    // ++++++++++++++++++++++++++++++++++++++
    private function editProduct($product){
        $productFields = $this->getProductFields($product);
        $preparedString = 'UPDATE product SET '
            . 'id=?, '
            . 'section_id=?, '
            . 'name=?, '
            . 'price=?, '
            . 'desription=?, '
            . 'photo=?, '
            . 'purpose_id=?, '
            . 'supplier_id=? WHERE id=?';
        $paramsTypes = "iisissiii";
        $paramValues = array(
            $productFields['id'],
            $productFields['section_id'],
            $productFields['name'],
            $productFields['price'],
            $productFields['desription'],
            $productFields['photo'],
            $productFields['purpose_id'],
            $productFields['supplier_id'],
            $productFields['id']
        );
        $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
    }
    // Обновить товары в БД с помощью распарсенного массива товаров
    // ++++++++++++++++++++++++++++++++++++++
    private function updateProducts($products){
        $productIdsFromDB = $this->getProductIdsDB();
        foreach ($products as $index => $product) {
            $ID = $product[0]['ID'];
            $CHARACTERISTICS = $product[5]['CHARACTERISTICS'];
            // если есть товар в БД - оновить поля
            if(in_array($ID, $productIdsFromDB)) {
                $this->editProduct($product);
            }
            // если нет - добавить товар
            else {
                $this->addProduct($product);
            }
        }
    }
    // Получить поля для товаров из распарсенного объекта товара
    // ++++++++++++++++++++++++++++++++++++++
    private function getProductFields($product) {
        $supplierIndex = 0;
        $purposeIndex = 1;
        $nameIndex = 1;
        $ID = $product[0]['ID'];
        $NAME = $product[1]['NAME'];
        $ID_SECTION = $product[2]['ID_SECTION'];
        $IMAGE = $product[3]['IMAGE'];
        $DESCRIPTION = $product[4]['DESCRIPTION'];
        $CHARACTERISTICS = $product[5]['CHARACTERISTICS'];
        $supplierName = $CHARACTERISTICS[$supplierIndex]['CHARACTERISTIC'][$nameIndex]['VALUE'];
        $purposeName = $CHARACTERISTICS[$purposeIndex]['CHARACTERISTIC'][$nameIndex]['VALUE'];
        $PRICE = 123;

        $preparedString = 'SELECT id FROM supplier WHERE name=?';
        $paramsTypes = "s";
        $paramValues = array($supplierName);
        $result = $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
        if($result)
            foreach($result as $row) {
                $SUPPLIER_ID = $row['id'];
                break;
            }
        $preparedString = 'SELECT id FROM purpose WHERE name=?';
        $paramsTypes = "s";
        $paramValues = array($purposeName);
        $result = $this->DBconn->preparedQuery($preparedString, $paramsTypes, $paramValues);
        if($result)
            foreach($result as $row) {
                $PURPOSE_ID = $row['id'];
                break;
            }
        return $fieldsArray = array('id' => $ID, 'section_id' => $ID_SECTION, 'name' => $NAME,
            'price' => $PRICE, 'desription' => $DESCRIPTION, 'photo' => $IMAGE,
            'purpose_id' => $PURPOSE_ID, 'supplier_id' => $SUPPLIER_ID);
    }



}