<?php
/**
 * Класс MainModel
 * Модель для главного раздела
 */

namespace application\models;
use application\core\Model;


class MainModel extends Model
{
    // Получить все разделы
    public function getSections() {
        $preparedString = 'SELECT * FROM section';
        return $this->DBconn->preparedQuery($preparedString);
    }

}