<?php

namespace application\core;
require_once $_SERVER['DOCUMENT_ROOT'] . '/application/include/connection.php';


/**
 * class Db
 * Класс для работы с бд, реализующий подключение по синглтону
 */
class Db
{
    private $connection;
    static private $instance = null;

    /**
     * Метод-конструктор для создания подключения к бд
     */
    private function __construct() {
        $this->connection = new \Mysqli(HOST, USER, PASS, DATABASE);
        $this->connection->set_charset("utf8");
        $this->connection->query("ALTER DATABASE zoo CHARACTER SET utf8 COLLATE utf8_unicode_ci;");
        $this->connection->query("ALTER TABLE purpose CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;");
    }

    /**
     * Метод для клонирования экземпляра.
     */

    private function __clone() {}

    /**
     * Метод, обеспечивающий единственное создание экземпляра класса Db
     * @return self::$instance (экемпляр класса)
     */

    static function getInstance()
    {
        if(self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    /**
     * Метод, для запросов
     * @param string $sql
     */

    public function query($sql){
        return $this->connection->query($sql);
    }

    /**
     * Выполнение подготовленного запроса к базе данных всех записей из
     * таблицы
     * @author PC16
     * @param string $preparedString имя таблицы передается здесь как часть строки
     * @param array $params параметры для заполнителей
     *                      (ключ - тип заполнителя, значение - значение для заполнителя)
     * @return mysqli_result Объект mysqli_result с результатами запроса
     */
    public function preparedQuery($preparedString, $types = NULL, $params = NULL) {
        if(!$params) {
            $result = $this->query($preparedString);
        }
        else {
            if($statement = $this->connection->prepare($preparedString)) {               
                $paramsArray = array();
                foreach($params as $key => $value) {
                    $paramsArray[$key] = &$params[$key];
                }
                $result_params = array_merge(array($types), $paramsArray);
                call_user_func_array(array($statement, 'bind_param'), $result_params);
                $statement->execute();
                $result = $statement->get_result();
                $statement->close();
            }
        }
        $output = [];
        if($result->num_rows > 0) {
            foreach($result as $row) {
                $output[] = $row;
            }
            return $output;
        }
        return NULL;
    }
}

