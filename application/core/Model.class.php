<?php
/**
 * Class Model
 * Главный класс модели
 */


namespace application\core;
use application\core\Db;


class Model
{
    public $DBconn;

    public function __construct()
    {
        $this->DBconn = Db::getInstance();
    }
}