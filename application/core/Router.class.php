<?php
/**
 * class Router
 * Класс для роутинга ссылок
 */

namespace application\core;
use application\controllers\BasketController;

class Router
{
    protected $routes = [];
    protected $params = [];
    private $basketController;
    /**
     * Функция-конструктор, подключающая маршруты и формирующая массив роута
     */

    function __construct()
    {
        $arr = require $_SERVER['DOCUMENT_ROOT'] . '/application/include/routes.php';
        foreach ($arr as $route => $params) {
            $this->add($route, $params);
        }
        $this->basketController = new BasketController();
        
    }

    /**
     * Функция для добавления маршрутов в нашу протектед переменную
     * @param $route (массив роутов)
     * @param $params (параметры массива роутов с соответсвующем контроллером и действием)
     */

    public function add($route,$params)
    {
        $route = preg_replace('/{([a-z]+):([^\}]+)}/', '(?P<\1>\2)', $route);
        $route = '#^' . $route . '$#';
        $this->routes[$route] = $params;
    }

    /**
     * Функция для проверки регуляркой есть ли у нас такой путь или нет
     * @return bool;
     */

    public function match(){
        $url = trim($_SERVER['REQUEST_URI'], '/');
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                foreach ($matches as $key => $match) {
                    if (is_string($key)) {
                        if (is_numeric($match)) {
                            $match = (int) $match;
                        }
                        $params[$key] = $match;
                    }
                }
                $this->params = $params;
                return true;
            }
        }
        return false;
    }

    /**
     * Функция запускающая всю систему, если маршрут найден и 404 если нет
     */

    public function run()
    {
        if ($this->match()) {
            $controller = 'application\controllers\\' . ucfirst($this->params['controller']) . 'Controller';
            if (class_exists($controller)) {
                $action = $this->params['action'].'Action';
                if (method_exists($controller, $action)) {
                    $controller = new $controller($this->params);
                    $controller->$action();
                } else {
                    echo 'Метод не найден';
                }
            } else {
                echo 'Класс не найден';
            }
        } else {
            if(isset($_POST)) {
                if(isset($_POST['command'])) {
                    $productId = trim(strip_tags($_POST['idTovar']));
                    if($_POST['command'] == 'addTovarToBasketInCatalog') {
                        $this->basketController->addProduct($productId, $_POST['collTovar']);
                        return;
                    }
                    if($_POST['command'] == 'updateTovar') {
                        $this->basketController->updateProduct($productId, $_POST['collTovar']);
                        return;
                    }
                    if($_POST['command'] == 'deleteTovar') {
                        $this->basketController->deleteProduct($productId);
                        return;
                    }
                    if($_POST['command'] == 'clearBasket') {
                        $this->basketController->clear();
                        return;
                    }
                }
            }
            else {
                echo 'Маршрут не найден';
            }
        }         
    }

}