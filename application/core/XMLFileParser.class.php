<?php

namespace application\core;

/**
 * Парсинг файла XML с данными для БД
 * @author PC16
 */
class XMLFileParser {
    private $sections;
    private $products;
    private $characteristicsDictionary;
    private $tagsSet;

    private $xmlParser;
    private $xmlString;

    private $tree;
    private $stack;
    private $previousStartTag;
    private $text;
    private $contentTags = array(
        'CATALOG',
        'SECTIONS',
        'SECTION',
        'ID',
        'NAME',
        'PRODUCTS',
        'PRODUCT',
        'ID_SECTION',
        'IMAGE',
        'DESCRIPTION',
        'CHARACTERISTICS',
        'CHARACTERISTIC',
        'VALUE',
        'CHARACTERISTICS_DICTIONARY'
    );

    function __construct($fileName) {
        $this->sections['SECTIONS'] = array();
        $this->products['PRODUCTS'] = array();
        $this->characteristicsDictionary['CHARACTERISTICS_DICTIONARY'] = array();
        $this->tagsSet = array();
        $this->xmlString = file_get_contents($fileName);
    }

    function __destruct() {
        if(is_resource($this->xmlParser)){
            xml_parser_free($this->xmlParser);
        }
        $this->sections = NULL;
        $this->products = NULL;
        $this->characteristicsDictionary = NULL;
        $this->tagsSet = NULL;
        $this->tree = NULL;
        $this->stack = NULL;
        $this->previousStartTag = NULL;
        $this->text = NULL;
        $this->xmlString = NULL;
    }


    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    /////////     PUBLIC
    public function getAllTags() {
        $this->xmlParser = xml_parser_create();
        $this->tagsSet = array();
        xml_set_element_handler($this->xmlParser, array(&$this, "onGetAllTags"), NULL);
        xml_set_character_data_handler($this->xmlParser, NULL);

        $this->initForParse();
        $this->parse();
        $this->showAllTags();
        $this->releaseParser($this->xmlParser);
    }

    public function getParsedArray() {
        $this->xmlParser = xml_parser_create();
        //xml_parser_set_option($xml_parser,XML_OPTION_TARGET_ENCODING,'UTF-8');
        xml_set_element_handler($this->xmlParser, array(&$this, "onStartTag"), array(&$this, "onEndTag"));
        xml_set_character_data_handler($this->xmlParser, array(&$this, "onText"));

        $this->initForParse();
        $this->parse();
        $this->showTree();
        $this->releaseParser($this->xmlParser);
    }

    public function getSeparateArrays() {
        $this->xmlParser = xml_parser_create();
        xml_set_element_handler($this->xmlParser, array(&$this, "onStartTag"), array(&$this, "onEndTagSepar"));
        xml_set_character_data_handler($this->xmlParser, array(&$this, "onText"));

        $this->initForParse();
        $this->parse();
        $this->releaseParser($this->xmlParser);
    }

    public function getProducts() {
        return $this->getCatalogCategory($this->products)['PRODUCTS'];
    }

    public function getSections() {
        return $this->getCatalogCategory($this->sections)['SECTIONS'];
    }

    public function getCaracteristicsDictionary() {
        return $this->getCatalogCategory($this->characteristicsDictionary)['CHARACTERISTICS_DICTIONARY'];
    }

    // получение списка поставщиков для продуктов
    public function getSuppliers() {
        $products = $this->getProducts();
        $supplierIndex = 0;
        $nameIndex = 1;
        $suppliers = array();
        foreach ($products as $index => $product) {
            $CHARACTERISTICS = $product[5]['CHARACTERISTICS'];
            $supplierName = $CHARACTERISTICS[$supplierIndex]['CHARACTERISTIC'][$nameIndex]['VALUE'];
            if (!in_array($supplierName, $suppliers)) {
                array_push($suppliers, $supplierName);
            }
        }
        return $suppliers;
    }

    // получение списка назначений для продуктов
    public function getPurposes() {
        $products = $this->getProducts();
        $purposeIndex = 1;
        $nameIndex = 1;
        $purposes = array();
        foreach ($products as $index => $product) {
            $CHARACTERISTICS = $product[5]['CHARACTERISTICS'];
            $purposeName = $CHARACTERISTICS[$purposeIndex]['CHARACTERISTIC'][$nameIndex]['VALUE'];
            if (!in_array($purposeName, $purposes)) {
                array_push($purposes, $purposeName);
            }
        }
        return $purposes;
    }

    public function productCaracteristicsToJSON($product){
        $CHARACTERISTICS = $product[5]['CHARACTERISTICS'];
        if($CHARACTERISTICS){
            $newArray = array();
            foreach($CHARACTERISTICS as $index => $c){
                $characteristic = $CHARACTERISTICS[$index]['CHARACTERISTIC'];
                foreach($characteristic as $id => $name){
                    if(key($characteristic[$id]) == 'ID'){
                        $characteristicKey = $characteristic[$id][key($characteristic[$id])];
                    }
                    else {
                        $characteristicValue = $characteristic[$id][key($characteristic[$id])];
                    }
                }
                $temp[$characteristicKey] = $characteristicValue;
                array_push($newArray, $temp);
                $temp = array();
            }
            return json_encode($newArray);
        }
        return NULL;
    }

    public function showProducts() {
        $this->showCatalogCategory($this->products);
    }

    public function showSections() {
        $this->showCatalogCategory($this->sections);
    }

    public function showCaracteristics() {
        $this->showCatalogCategory($this->characteristicsDictionary);
    }




    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    /////////     PRIVATE
    private function showAllTags() {
        if(!is_null($this->tagsSet)){
            foreach ($this->tagsSet as $tag) {
                echo ". . . . . . . . . . " . $tag."<br>";
            }
        }
    }

    private function showTree() {
        echo '<pre>';
        var_dump($this->tree);
        echo '</pre>';
    }

    private function showCatalogCategory(array $category) {

        if ($category && count($category[key($category)]) > 0) {
            switch(key($category)) {
                case "SECTIONS":
                    $categoryItemName = "SECTION";
                    break;
                case "PRODUCTS":
                    $categoryItemName = "PRODUCT";
                    break;
                case "CHARACTERISTICS_DICTIONARY":
                    $categoryItemName = "CHARACTERISTIC";
                    break;
            }

            $counter = 1;
            foreach($category[key($category)] as $categoryItem) {
                echo '<br>';
                echo "[$categoryItemName] № $counter";
                echo '<br><br>';
                var_dump($categoryItem);
                echo '<hr>';
                ++$counter;
            }
        }
        else {
            echo "<br> Отсутствуют данные для вывода <br><hr>";
        }
    }

    private function getCatalogCategory(array $category) {
        if ($category && count($category[key($category)]) > 0) {
            return $category;
        }
        return array();
    }

    private function initForParse(){
        $this->tree = array();
        $this->stack[count($this->stack)] = &$this->tree;
        $this->text = "";
        $this->previousStartTag = NULL;
    }

    private function releaseParser($parser) {
        xml_parser_free($parser);
        $this->tree = NULL;
        $this->stack = NULL;
        $this->text = NULL;
        $this->previousStartTag = NULL;
    }

    private function parse() {
        if($this->xmlString) {
            if (!xml_parse($this->xmlParser, $this->xmlString)) {
                $errorText = xml_error_string(xml_get_error_code($this->xmlParser));
                $errorLine = xml_get_current_line_number($this->xmlParser);
                echo "<br> Ошибка обработки XML: " . $errorText;
                echo " на строке " . $errorLine;
            }
        }
        else {
            echo "<br> Отсутствуют данные для разбора";
        }
    }

    private function onGetAllTags($parser, $tagName, $attrs) {
        if(!in_array($tagName, $this->tagsSet)){
            array_push($this->tagsSet, $tagName);
        }
    }

    private function onText($parser, $data) {
        $this->text .= trim((string)$data);
    }

    private function onStartTag($parser, $tagName, $attrs) {
        if (in_array($tagName, $this->contentTags)){
            $this->previousStartTag = $tagName;
            $newContainer = array();
            $newContainer[$tagName] = array();

            $lastContainer = &$this->stack[count($this->stack)-1];
            $lastContainer[count($lastContainer)] = &$newContainer;
            $this->stack[count($this->stack)] = &$newContainer[$tagName];
        }
        else {
            $this->text .= '<' . strtolower(trim((string)$tagName)) . '>';
        }
    }

    private function onEndTag($parser, $tagName) {
        if (in_array($tagName, $this->contentTags)){
            array_pop($this->stack);

            if($this->previousStartTag == $tagName) {
                $lastContainer = &$this->stack[count($this->stack)-1];
                $lastContainerIndex = count($lastContainer);
                $lastContainer[$lastContainerIndex-1][$tagName] = $this->text;
                $this->text = "";
            }
        }
        else {
            if (strtolower(trim((string)$tagName)) == "br") {
                $this->text .= '<' . strtolower(trim((string)$tagName)) . '>';
            }
            else {
                $this->text .= '</' . strtolower(trim((string)$tagName)) . '>';
            }
        }
    }

    private function onEndTagSepar($parser, $tagName) {
        if (in_array($tagName, $this->contentTags)){
            array_pop($this->stack);

            if($this->previousStartTag == $tagName) {
                $lastContainer = &$this->stack[count($this->stack)-1];
                $lastContainerIndex = count($lastContainer);
                $lastContainer[$lastContainerIndex-1][$tagName] = $this->text;

                $this->text = "";
            }
            else {
                $parentContainer = &$this->stack[count($this->stack)-1];
                $parentContainerKey = key($parentContainer[0]);

                switch ($parentContainerKey) {
                    case "SECTION":
                        array_push($this->sections["SECTIONS"], $parentContainer[count($parentContainer)-1]["SECTION"]);
                        break;
                    case "PRODUCT":
                        array_push($this->products["PRODUCTS"], $parentContainer[count($parentContainer)-1]["PRODUCT"]);
                        break;
                    case "CHARACTERISTIC":
                        if(key_exists("NAME", $parentContainer[0]["CHARACTERISTIC"][1])) {
                            array_push($this->characteristicsDictionary["CHARACTERISTICS_DICTIONARY"],
                                $parentContainer[count($parentContainer)-1]["CHARACTERISTIC"]);
                            break;
                        }
                }
            }
        }
        else {
            if (strtolower(trim((string)$tagName)) == "br") {
                $this->text .= '<' . strtolower(trim((string)$tagName)) . '>';
            }
            else {
                $this->text .= '</' . strtolower(trim((string)$tagName)) . '>';
            }
        }
    }
}
