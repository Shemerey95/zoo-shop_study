<?php
/**
 * Class View
 * Главный класс видов
*/

namespace application\core;

class View
{
    public $path;
    public $route;
    public $layout = 'default';

    /**
     * Функция-конструктор, являющаяся общим конструктором для всех видов
     * @param $route (наши роуты (соттветсвующий экшн и контроллер)
     */

    public function __construct($route)
    {
        $this->route = $route;
        $this->path = $route['controller'] . '/' . $route['action'];
    }

    /**
     * Функция подключающая соответсвующий вид для страницы
     * @param $title - название страницы
     * @param $vars - параметры для формирования страницы
     */

    public function render($title, $vars = [])
    {
        if (!isset($this->route['id'])) {
            ob_start();
            require 'application/views' . '/' . $this->path . '.php';
            $content = ob_get_clean();
            require 'application/views/layouts' . '/' . $this->layout . '.php';
        } else {
            ob_start();
            require 'application/views/catalog/cartochka.php';
            $content = ob_get_clean();
            require 'application/views/layouts' . '/' . $this->layout . '.php';
        }

    }


}
