<?php
/**
 * class Pagination
 * Класс пагинации
 */

namespace application\core;

class Pagination
{
    private $max = 5; // Количество страниц
    private $route; // Наш путь
    private $current_page; // Текущая страница
    private $total; // Всего записей
    private $limit; // Записей на страницу

    public function __construct($route, $total, $limit = 10) {
        $this->route = $route;
        $this->total = $total;
        $this->limit = $limit;
        $this->amount = $this->amount();
        $this->setCurrentPage();
    }

    /**
     * Функция получения (вывода) пагинации на страницу.
     */

    public function get() {
        $links = null;
        $limits = $this->limits();
        $html = '<ul class="content__pagination">';
        for ($page = $limits[0]; $page <= $limits[1]; $page++) {
            if ($page == $this->current_page) {
                $links .= '<li class="content__pagination_element content__pagination_element-selected">'.$page.'</span></li>';
            } else {
                $links .= $this->generateHtml($page);
            }
        }
        if (!is_null($links)) {
            if ($this->current_page > 1) {
                $links = $this->generateHtml($this->current_page - 1, '<i class="fas fa-angle-left"></i>').$links;
                $links = $this->generateHtml(1, '<i class="fas fa-angle-double-left"></i>').$links;
            }
            if ($this->current_page == 1){
                $links = $this->generateHtmlArrow(1, '<i class="fas fa-angle-left"></i>').$links;
                $links = $this->generateHtmlArrow(1, '<i class="fas fa-angle-double-left"></i>').$links;
            }
            if ($this->current_page < $this->amount) {
                $links .= $this->generateHtml($this->current_page + 1, '<i class="fas fa-angle-right"></i>');
                $links .= $this->generateHtml($this->amount, '<i class="fas fa-angle-double-right"></i>');
            }
            if ($this->current_page == $this->amount) {
                $links .= $this->generateHtmlArrow($this->amount, '<i class="fas fa-angle-right"></i>');
                $links .= $this->generateHtmlArrow($this->amount, '<i class="fas fa-angle-double-right"></i>');
            }
        }

        $html .= $links.' </ul>';
        $array_html = [];

        $array_html[] = $html;
        return $html;
    }

    /**
     * Функция генерации верстки пагинации
     * @param $page;
     * @param $text;
     */

    private function generateHtml($page, $text = null) {
        if (!$text) {
            $text = $page;
        }
        return '<a href="/'.lcfirst($this->route['action']).'/page='.$page.'"><li class="content__pagination_element">'.$text.'</li></a>';
    }

    /**
     * Функция генерации верстки для стрелок пагинации
     * @param $page;
     * @param $text;
     */

    private function generateHtmlArrow($page, $text = null) {
        if (!$text) {
            $text = $page;
        }
        return '<li class="content__pagination_element_shadow">'.$text.'</li>';
    }

    /**
     * Функция получения получающая начальную и конечную записть
     * @return $start - номер начальной записи
     * @return $end - номер крайней записи
     */

    private function limits() {
        $left = $this->current_page - round($this->max / 2);
        $start = $left > 0 ? $left : 1;
        if ($start + $this->max <= $this->amount) {
            $end = $start > 1 ? $start + $this->max : $this->max;
        }
        else {
            $end = $this->amount;
            $start = $this->amount - $this->max > 0 ? $this->amount - $this->max : 1;
        }
        return array($start, $end);
    }

    /**
     * Функция для установления текущей страницы
     */

    private function setCurrentPage() {
        if (isset($this->route['page'])) {
            $currentPage = $this->route['page'];
        } else {
            $currentPage = 1;
        }
        $this->current_page = $currentPage;
        if ($this->current_page > 0) {
            if ($this->current_page > $this->amount) {
                $this->current_page = $this->amount;
            }
        } else {
            $this->current_page = 1;
        }
    }

    /**
     * Функция для установления количества страниц
     */

    private function amount() {
        return ceil($this->total / $this->limit);
    }
}