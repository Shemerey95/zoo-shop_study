<?php
/**
 * class Controller
 * Главный контроллер классов
 */

namespace application\core;
use application\core\View;
use application\controllers\BasketController;

abstract class Controller
{
    public $route;
    public $view;
    public $model;
    
    private $basketController;

    /**
     * Функция-конструктор, являющаяся общим конструктором для всех контроллеров, создает вид и соответсвующую странице модель
     * @param $route (наши роуты (соттветсвующий экшн и контроллер)
     */

    public function __construct($route)
    {
        session_start();
        $this->route = $route;
        $this->view = new View($route);
        $this->model = $this->loadModel($route['controller']);
        $this->basketController = new BasketController();
        $this->basketController->run();
    }

    /**
     * Функция, создающая соответсвующую данной странице модель
     * @name (имя нашей модели)
     */

    public function loadModel($name) {
        $path = 'application\models\\' . ucfirst($name) . 'Model';
        if (class_exists($path)) {
            return new $path;
        }
    }


}
