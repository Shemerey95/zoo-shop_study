<?php
/**
 * class BasketController
 * Главный контроллер для корзины
 */

namespace application\controllers;
use application\core\Controller;
use application\models\BasketModel;
use application\core\View;

class BasketController extends Controller
{
    private static $countProducts;
    private static $totalPrice;
    private $basketModel;
    private $sessionId;
    
    public function __construct() {
        $this->basketModel = new BasketModel();
    }
    /*
     * Инициализация корзины
     */
    public function run() {
        $this->sessionId = $this->getSessionId();
        $userBasket = $this->basketModel->getBasket($this->sessionId);
        
        if (!$userBasket) {
            $this->basketModel->addBasket($this->sessionId, json_encode(array(), JSON_UNESCAPED_UNICODE));
        }
        else {
            $this->setCount();
            $this->setTotal();
        }
    }
    /*
     * Получение ID сессии из куки
     */
    private function getSessionId(){
        if(!isset($_COOKIE['USER_ID'])) {
            $cookieTime = 3600;
            $sessionId = uniqid('sid');
            setcookie('USER_ID', $sessionId, time() + $cookieTime);
        }
        $sessionId = trim(strip_tags($_COOKIE['USER_ID']));
        return $sessionId;
    }
    /*
     * Расчет количества товаров в корзине
     * @return integer Количество товаров в корзине
     */
    public function setCount() {
        return self::$countProducts = $this->basketModel->productItemsCount();
    }
    /*
     * Расчет общей суммы по корзине
     * @return integer Количество товаров в корзине
     */
    public function setTotal() {
        return self::$totalPrice = $this->basketModel->basketTotalPrice();
    }
    /*
     * Получение количества товаров в корзине
     * @return integer Количество товаров в корзине
     */
    public static function getCount() {
        if(self::$countProducts)
            return self::$countProducts;
        return 0;
    }
    /*
     * Получение общей суммы по корзине
     * @return integer Количество товаров в корзине
     */
    public static function getTotal() {
        if(self::$totalPrice)
            return self::$totalPrice;
        return 0;
    }
    /*
     * Добавление товара в корзину
     * @param integer $id Id товара
     * @param integer $count количество товара
     */
    public function addProduct($id, $count) {
        if($count > 0){
            $this->basketModel->addProduct($id, $count);
            echo json_encode(array(
                'coll' => self::setCount(),
                'cost' => self::setTotal()
            ), JSON_UNESCAPED_UNICODE);
        }
    }
    /*
     * Редактирование товара в корзине
     * @param integer $id Id товара
     * @param integer $count количество товара
     */
    public function updateProduct($id, $count) {
        if($count > 0){
            $productTotal = $this->basketModel->updateProduct($id, $count);
            echo json_encode(array(
                'coll' => self::setCount(), 
                'cost' => self::setTotal(),
                'productTotal' => $productTotal
            ), JSON_UNESCAPED_UNICODE);
        }
    }
    /*
     * Удаление товара из корзины
     * @param integer $id Id товара
     */
    public function deleteProduct($id) {
        $total = $this->basketModel->deleteProduct($id);
        echo json_encode(array(
            'coll' => self::setCount(), 
            'cost' => $total
        ), JSON_UNESCAPED_UNICODE);    
    }
    /*
     * Удаление корзины
     */
    public function clear() {
        $total = $this->basketModel->clear();
        echo json_encode(array(
            'coll' => 0, 
            'cost' => 0
        ), JSON_UNESCAPED_UNICODE);    
    }
    /*
     * Отображение страницы корзины
     */
    public function indexAction() {   
        $this->run();
        $vars = $this->basketModel->getProductsList($this->sessionId);
        $basketView = new View(array('controller' => 'basket', 'action' => 'index'));
        $basketView->render('Корзина сайта', $vars);  
    } 
}
