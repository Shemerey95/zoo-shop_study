<?php
/**
 * class CatalogController
 * Главный контроллер для каталога
 */

namespace application\controllers;
use application\core\Controller;
use application\core\Pagination;

class CatalogController extends Controller
{
    /**
     * Функция-действие для страницы каталога ,запускающая подключение видов
     */

    public function DogsAction()
    {
        $id = $this->model->getIdSectionByName($this->route['action']);
        $count_array=$this->model->getProductsByIdSection($id);
        $pagination = new Pagination($this->route,count($count_array),20);
        $vars=[
            'pagination' => $pagination->get(),
            'list' => $this->model->postList($this->route,$id),
            'route' => $this->route,
        ];


        if (!isset($this->route['id'])) {
            $this->view->render('Каталог корма для собак',$vars);

        } else {

            $similars = $this->model->getSimilarProducts($id);
            $good = $this->model->getProductById($this->route['id']);


            $vars=[
                'list' => $good,
                'route' => $this->route,
                'similars' => $similars,
            ];
            $this->view->render('Карточка корма для собак',$vars);
        }
    }

    /**
     * Функция-действие для страницы каталога ,запускающая подключение видов
     */

    public function CatsAction()
    {
        $id = $this->model->getIdSectionByName($this->route['action']);
        $count_array=$this->model->getProductsByIdSection($id);
        $pagination = new Pagination($this->route,count($count_array),20);
        $vars=[
            'pagination' => $pagination->get(),
            'list' => $this->model->postList($this->route,$id),
            'route' => $this->route,
        ];


        if (!isset($this->route['id'])) {
            $this->view->render('Каталог корма для кошек',$vars);

        } else {

            $similars = $this->model->getSimilarProducts($id);
            $good = $this->model->getProductById($this->route['id']);


            $vars=[
                'list' => $good,
                'route' => $this->route,
                'similars' => $similars,
            ];
            $this->view->render('Карточка корма для кошек',$vars);
        }
    }


    /**
     * Функция-действие для страницы каталога ,запускающая подключение видов
     */

    public function RodentsAction()
    {
        $id = $this->model->getIdSectionByName($this->route['action']);
        $count_array=$this->model->getProductsByIdSection($id);
        $pagination = new Pagination($this->route,count($count_array),20);
        $vars=[
            'pagination' => $pagination->get(),
            'list' => $this->model->postList($this->route,$id),
            'route' => $this->route,
        ];


        if (!isset($this->route['id'])) {
            $this->view->render('Каталог корма для грызунов',$vars);

        } else {

            $similars = $this->model->getSimilarProducts($id);
            $good = $this->model->getProductById($this->route['id']);


            $vars=[
                'list' => $good,
                'route' => $this->route,
                'similars' => $similars,
            ];
            $this->view->render('Карточка корма для грызунов',$vars);
        }
    }

    /**
     * Функция-действие для страницы каталога ,запускающая подключение видов
     */

    public function BirdsAction()
    {
        $id = $this->model->getIdSectionByName($this->route['action']);
        $count_array=$this->model->getProductsByIdSection($id);
        $pagination = new Pagination($this->route,count($count_array),20);
        $vars=[
            'pagination' => $pagination->get(),
            'list' => $this->model->postList($this->route,$id),
            'route' => $this->route,
        ];


        if (!isset($this->route['id'])) {
            $this->view->render('Каталог корма для птиц',$vars);

        } else {

            $similars = $this->model->getSimilarProducts($id);
            $good = $this->model->getProductById($this->route['id']);


            $vars=[
                'list' => $good,
                'route' => $this->route,
                'similars' => $similars,
            ];
            $this->view->render('Карточка корма для птиц',$vars);
        }
    }

    public function InitAction(){
        $this->model->initCatalog();
        echo "База проинициализирована";
    }


}
