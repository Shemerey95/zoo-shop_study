<?php

/**
 * class MainController
 * Главный контроллер для main раздела
 */

namespace application\controllers;
use application\core\Controller;

class MainController extends Controller

    /**
     * Функция-действие для главной страницы ,запускающая подключение видов
     */
{
    public function indexAction()
    {
        $sections = $this->model->getSections();
        $this->view->render('Главная страница', $sections);  
    }
}


