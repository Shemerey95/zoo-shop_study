<?php

use application\core\Db;

$mysqli = Db::getInstance();
$result_routs = $mysqli->query("SELECT `route` FROM `section`");
$result_routes_array = [];
while ($route = $result_routs->fetch_array()){
    $result_routes_array[] = $route[0];
}


$routes = [
    '' => [
        'controller' => 'main',
        'action' => 'index',
    ],

    'init' => [
        'controller' => 'catalog',
        'action' => 'init',
    ],

    'basket' => [
        'controller' => 'basket',
        'action' => 'index',
    ],

    $result_routes_array['0'] . '/{id:\d+}' => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['0']),
    ],

    $result_routes_array['0'] . '/page={page:\d+}' => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['0']),
    ],

    $result_routes_array['0'] => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['0']),
    ],

    $result_routes_array['1'] . '/{id:\d+}' => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['1']),
    ],

    $result_routes_array['1'] . '/page={page:\d+}' => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['1']),
    ],

    $result_routes_array['1'] => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['1']),
    ],

    $result_routes_array['2'] . '/{id:\d+}' => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['2']),
    ],

    $result_routes_array['2'] => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['2']),
    ],

    $result_routes_array['2'] . '/page={page:\d+}' => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['2']),
    ],

     $result_routes_array['3'] . '/{id:\d+}' => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['3']),
    ],

    $result_routes_array['3'] => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['3']),
    ],

     $result_routes_array['3'] . '/page={page:\d+}' => [
        'controller' => 'catalog',
        'action' => ucfirst($result_routes_array['3']),
    ]
];





return $routes;