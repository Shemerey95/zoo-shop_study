<content>
<div class="container">
	<div class="row content__main">

		<div class="col-12">
			<h1 class="content__head">Каталог</h1>
		</div>

        <? foreach($vars as $key_1 => $value_1): ?>
                <div class="col-3 content__main_column">
                    <a href="<?=$value_1['route']?>" class="content__main-column-element" style="background: url('<?=$value_1['photo']?>') no-repeat;">
                        <span class="content__main-column-element_trapezium"><span><?=$value_1['name']?></span></span>
                        <span class="content__main_column-more">Подробнее</span>
                    </a>
                </div>
        <? endforeach; ?>

	</div>

</div>
</content>



