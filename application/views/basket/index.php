<?php
    use application\controllers\BasketController;
?>

<content>
	<div class="container">
		<div class="content__crumbs">
			<a href="#" class="content__crumbs_crumb">Главная</a><span> > </span>
			<div class="content__crumbs_crumb">Корзина</div>
		</div>
		<p class="content__section-name">Корзина</p>
		<table class="content__basket-table">
  			<thead>
    		<tr class="content__basket-table_thead">
			    <th>ID</th>
			    <th>Фото</th>
			    <th>Наименование</th>
			    <th>Цена</th>
			    <th>Количество</th>
			    <th>Сумма</th>
			    <th></th>
		    </tr>
  			</thead>
  			<tbody>
                            <?php foreach ($vars as $product):?>
    		<tr data-id="<?=$product['id']?>">
     			<th><?=$product['id']?></th>
                        <td><div class='content__tovar_img' style='background: url("<?=$product['photo']?>") no-repeat;background-position: center;background-size: contain'></div></td>
		      	<td><?=$product['name']?></td>
		      	<td><?=$product['price']?> р.</td>
		      	<td>
		      		<div class="content__cartochka-tovar_coll stepper stepper--style-2 js-spinner">
					    <input type="number" min="0" max="100" step="1" data-id="<?=$product['id']?>" value="<?=$product['count']?>" class="stepper__input content__basket-table_tovar-coll">
					    <div class="stepper__controls">
					        <button type="button" spinner-button="up">+</button>
					        <button type="button" spinner-button="down">−</button>
					    </div>
					</div>
		      	</td>
                        <td class="td-total<?=$product['id']?>"><?=$product['total_price']?> р.</td>
		      	<td><a href="#" class="content__basket-table_tovar-delete">&#215;</a></td>
    		</tr>
                <?php endforeach; ?>
  			</tbody>
		</table>
		<div class="row content__basket-table_row-bottom">
			<div class="col-2">
				<a href="#" class="content__basket-table_basket-clear">Очистить корзину</a>
			</div>
			<div class="col-8"></div>
			<div class="col-2 ">
				Итого: <span><span class="content__basket__prise"><?=BasketController::getTotal()?></span> ₽</span>
			</div>
		</div>
	</div>
</content>
