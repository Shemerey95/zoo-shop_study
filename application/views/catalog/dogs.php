<!-- content begin -->
			<content>
			<div class="container">
				<div class="content__crumbs">
                        <a href = "/" class="content__crumbs_crumb">Главная<a/><span> > </span>
                        <a href = "/" class="content__crumbs_crumb">Каталог</a><span> > </span>
                        <div class="content__crumbs_crumb">Корм собакам</div>
				</div>
				<p class="content__section-name">Корм собакам</p>

                <div class="row content__catalog_row">
                    <? foreach ($vars['list'] as $key_1 => $val_1): ?>

                        <?php $ulr = $val_1['photo'] ?>

                        <div class="col-xl content__tovar-colummn">
                            <div class="content__tovar">
                                <div class="content__tovar_name"><?=$val_1['name']?></div>
                                <a href="/<?=lcfirst($vars['route']['action']) . '/' . $val_1['id']?>">
                                    <div class='content__tovar_img' style='background: url("<?=$ulr?>") no-repeat;background-position: center;background-size: contain;'></div>
                                </a>
                                <div class="content__tovar_cost"><?=$val_1['price']?></div>
                                <div class="content__tovar_buy" data-id="<?=$val_1['id']?>">
                                    <div class="content__tovar_buy-img"></div>
                                    <div class="content__tovar_buy-text">Купить</div>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
                    <? if (empty($vars['list'])): ?>
                        <div class="error_trade">Товаров больше нет</div>
                    <? endif; ?>

                <?php echo $vars['pagination'];?>

			</div>
		</content>

		<!-- content end -->
