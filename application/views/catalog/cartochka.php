<content>
	<div class="container">
		<div class="content__crumbs">
			<div class="content__crumbs_crumb">Главная</div><span> > </span>
			<div class="content__crumbs_crumb">Каталог</div><span> > </span>
			<div class="content__crumbs_crumb">Корм собакам</div><span> > </span>
			<div class="content__crumbs_crumb"><?=$vars['list']['name']?></div>
		</div>

     <?php if (isset($vars['list'])): ?>

		<p class="content__section-name"><?=$vars['list']['name']?></p>
		<div class="row content__row_cartochka-tovar">
			<div class="col-4 content__cartochka-tovar">
				<div class="content__cartochka-tovar_img-wrapper">
					<div style='background: url("<?=$vars['list']['photo']?>") no-repeat;background-position: center;' class="content__cartochka-tovar_img "></div>
				</div>
			</div>
			<div class="col-8 content__cartochka-tovar_description">
				<div class="content__cartochka-tovar_description-wrapper">
					<h2 class="content__cartochka-tovar_cost">Цена: <span><?=$vars['list']['price']?>₽</span></h2>
					<div class="content__cartochka-tovar_description-text">
                        <?=substr(strip_tags($vars['list']['desription']),0,1000)?>
					</div>
					<div class="content__cartochka-tovar_coll stepper stepper--style-2 js-spinner">
					    <input id="content__cartochka-tovar_coll-input" type="number" min="0" max="100" step="1" value="0" class="stepper__input">
					    <div class="stepper__controls">
					        <button type="button" spinner-button="up">+</button>
					        <button type="button" spinner-button="down">−</button>
					    </div>
					</div>
					<div class="content__cartochka-tovar_to-basket" data-id="<?=$vars['list']['id']?>">
						<div class="content__tovar_buy-img"></div>
						<div class="content__tovar_buy-text">В корзину</div>
					</div>
				</div>
			</div>
		</div>
		<p class="content__section-name content__cartochka-tovar_specifications">Характеристики</p>
		<div class="row content__cartochka-tovar_purpose">
			<div class="col-auto content__cartochka-tovar_purpose-text">Назначение</div>
			<div class="col content__cartochka-tovar_line-column">
				<div class="content__cartochka-tovar_line"></div>
			</div>
			<div class="col-auto content__cartochka-tovar_purpose-name"><?=$vars['list']['purpose']?></div>
		</div>
		<div class="row content__cartochka-tovar_manufacturer">
			<div class="col-auto content__cartochka-tovar_manufacturer-text">Производитель</div>
			<div class="col content__cartochka-tovar_line-column">
				<div class="content__cartochka-tovar_line"></div>
			</div>
			<div class="col-auto content__cartochka-tovar_manufacturer-name"><?=$vars['list']['supplier']?></div>
		</div>


		<p class="content__section-name content__cartochka-tovar_similar-goods">Похожие товары</p>
		<div class="row content__cartochka-tovar_similar-goods-row">
                <? foreach ($vars['similars'] as $key => $value): ?>
                    <div class="col-xl content__tovar-colummn">
                        <div class="content__tovar">
                            <div class="content__tovar_name"><?=$value['name']?></div>
                            <a href="/<?=lcfirst($vars['route']['action']) . '/' . $value['id']?>">
                                <div class='content__tovar_img' style='background: url("<?=$value['photo']?>") no-repeat;background-position: center; background-size: contain'></div>
                            </a>
                            <div class="content__tovar_cost"><?=$value['price']?></div>
                            <div class="content__tovar_buy similar" data-id="<?=$vars['list']['id']?>">
                                <div class="content__tovar_buy-img"></div>
                                <div class="content__tovar_buy-text">Купить</div>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>



            <?php else: ?>
                <div class="error_trade">Такого товара нет</div>
            <?php endif; ?>

		</div>
	</div>
</content>
