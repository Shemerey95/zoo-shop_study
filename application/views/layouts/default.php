<?php 
    use application\controllers\BasketController;
?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<title><? echo  $title ?></title>
		<link href="faviconka_ru_1133.png" rel="shortcut icon" type="image/png">

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
		<link rel="stylesheet" href="/public/js/stepper/css/main.css">

		<link rel="stylesheet" href="/public/css/style.css">
		<link rel="stylesheet" href="/public/css/adaptive.css">

		<script src="/public/js/jquery3.3.1.js" defer></script>
		<script src="/public/js/scripts.js" defer></script>
		<script src="/public/js/stepper/js/stepper.js" defer></script>
		<link rel="stylesheet" href="/public/css/reset.css">
	</head>

	<body>
		<!-- Header begin -->
		<header>
			<div class="container">
				<div class="row header__row">
					<div class="col-xl-2 header__col col body__logo">
						<a href="/"><img src="/public/img/logo.png" alt="зоомаркет" class="header__logo_img"></a>
					</div>
					<div class="col-xl-7">
					</div>

					<div class="col-xl-3 header__col header__cart">
						<img src="/public/img/cart.png" alt="корзина" class="header__logo_cat" id="header__logo_cat">
                                                <div class="header__cart_shape"><span><?=BasketController::getCount()?></span></div>
						<p>корзина</p><br/>
                                                <div class="header__cart_sum">Cумма: <span><?=BasketController::getTotal()?> р</span></div>
					</div>
				</div>
			</div>
		</header>
		<!-- Header end -->

		<!-- Nav begin -->
		<nav class="nav">
			<div class="container">
				<ul>
					<li><a href="/" title="Главная">Главная</a></li>
					<li><a href="/dogs" title="Корм собакам">Корм собакам</a></li>
					<li><a href="/cats" title="Корм кошкам">Корм кошкам</a></li>
					<li><a href="/rodents" title="Корм грызунам">Корм грызунам</a></li>
					<li><a href="/birds" title="Корм птицам">Корм птицам</a></li>
				</ul>
			</div>
		</nav>
		<!-- Nav end -->

        <!-- alert begin -->
        <div class="alert_modal"><!-- Сaмo oкнo -->
            <span class="modal_close"></span> <!-- Кнoпкa зaкрыть -->
            <span class="alert_head">Товар успешно добавлен</span>
        </div>

        <!-- alert end -->

		<?= $content; ?>

		<!-- footer begin -->
		<footer>
			<div class="container">
				<div class="row footer__row">
					<div class="col-xl-2 footer__logo">
						<a href="#"><img src="/public/img/logo.png" alt="зоомаркет" class="footer__logo_img"></a>
					</div>
					<div class="col-xl-2 footer__column">
						<a href="#">Каталог:</a>
					</div>
					<div class="col-xl-2 footer__column">
						<a href="/dogs">Собакам</a>
					</div>
					<div class="col-xl-2 footer__column">
						<a href="/cats">Кошкам</a>
					</div>
					<div class="col-xl-2 footer__column">
						<a href="/rodents">Грызунам</a>
					</div>
					<div class="col-xl-2 footer__column">
						<a href="/birds">Птицам</a>
					</div>
				</div>
			</div>
		</footer>
		<!-- footer end -->
	</body>
</html>
