<?php
header('Content-Type: text/html; charset=utf-8');
use application\core\Router;

/*Автозагрузка классов*/

spl_autoload_register(function ($class){
    $path = str_replace('\\', '/', $class . '.class.php');
    if (file_exists($path)){
        require_once $path;
    }
});

/*Создание экземпляра класса роутера*/

$router = new Router();
$router->run();